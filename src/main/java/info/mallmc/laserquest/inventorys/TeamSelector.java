package info.mallmc.laserquest.inventorys;

import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.framework.util.ItemUtil;
import info.mallmc.laserquest.LaserQuest;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Rushmead
 */
public class TeamSelector extends MallInventory {

    @Override
    public String getName() {
        return "Team Selector";
    }

    @Override
    public void open(Player p) {
        Inventory inv = Bukkit.createInventory(p, 9, getName());
        ItemStack red = ItemUtil.createItemSpecial(Material.WOOL, DyeColor.RED.getData(), "&4Red Team");
        ItemStack green = ItemUtil.createItemSpecial(Material.WOOL, DyeColor.GREEN.getData(), "&aGreen Team");
        inv.setItem(3, red);
        inv.setItem(5, green);

        p.openInventory(inv);
    }

    @Override
    public void click(Player p, int slot) {
        if (slot == 3) {
            if (LaserQuest.getInstance().getTeamManager().hasTeam(p)) {
                if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getName() == "Red") {

                } else {
                    LaserQuest.getInstance().getTeamManager().getTeamByName("Green").removePlayer(p);
                }
            }
            LaserQuest.getInstance().getTeamManager().getTeamByName("Red").addPlayer(p);
            Messaging.sendMessage(p, "laserquest.game.general.choseTeam", LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getColor(), LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getName());
        } else if (slot == 5) {
            if (LaserQuest.getInstance().getTeamManager().hasTeam(p)) {
                if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getName() == "Green") {

                } else {
                    LaserQuest.getInstance().getTeamManager().getTeamByName("Red").removePlayer(p);
                }
            }
            LaserQuest.getInstance().getTeamManager().getTeamByName("Green").addPlayer(p);
            Messaging.sendMessage(p, "laserquest.game.general.choseTeam", LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getColor(), LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getName());
        }
        p.closeInventory();
    }

}
