package info.mallmc.laserquest.inventorys.gunbuilder;

import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.framework.util.ItemUtil;
import info.mallmc.laserquest.game.guns.Gun;
import java.util.ArrayList;
import java.util.List;
import info.mallmc.laserquest.inventorys.AnvilGUI;
import info.mallmc.laserquest.inventorys.LaserQuestInventorys;
import info.mallmc.laserquest.player.LaserPlayer;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Rushmead
 */
public class GunBuilderMain extends MallInventory {

    @Override
    public String getName() {
        return "Gun Builder";
    }

    @Override
    public void open(Player p) {
        Inventory inv = Bukkit.createInventory(p, 54, getName());
        LaserPlayer lp = LaserPlayer.getPlayer(p.getName());
        Gun g = lp.getGun();
        ItemStack barrel = ItemUtil.createItem(g.getBarrel().getItem().getType(), "&a" + g.getBarrel().getName(), "&6Stats: " + g.getBarrel().getDamage() * 2 + " seconds", "&6Help: Amount of Reactivation time for other players");
        ItemStack gunCase = ItemUtil.createItem(g.getGunCase().getItem().getType(), "&a" + g.getGunCase().getName(), "&6The appearance of the gun");
        ItemStack cooler = ItemUtil.createItem(g.getCooler().getItem().getType(), "&a" + g.getCooler().getName(), "&6Stats: " + g.getCooler().getCooldown() + " seconds", "&6The Amount of cooldown time");
        ItemStack laser = ItemUtil.createItem(g.getLaser().getItem().getType(), g.getLaser().getItem().getData().getData(), "&6" + g.getLaser().getName(), "&6The Color of your laser and Firework");
        ItemStack name = ItemUtil.createItem(Material.NAME_TAG, "&5" + g.getName(), "&aClick to change your gun name");
        ItemStack shop = ItemUtil.createItem(Material.EMERALD, "&5Shop", "&aBrowse for new parts");
        ItemStack gun = new ItemStack(g.getGunCase().getMaterial());
        ItemMeta gunMeta = gun.getItemMeta();
        gunMeta.setDisplayName(ChatColor.GOLD + g.getName());
        List<String> lore = new ArrayList<>();
        lore.add(Messaging.colorizeMessage("&4Barrel: &6&l" + g.getBarrel().getName()));
        lore.add(Messaging.colorizeMessage("&4Cooler: &6&l" + g.getCooler().getName()));
        lore.add(Messaging.colorizeMessage("&4Case: &6&l" + g.getGunCase().getName()));
        lore.add(Messaging.colorizeMessage("&4Laser: &6&l" + g.getLaser().getName()));
        gunMeta.setLore(lore);
        gun.setItemMeta(gunMeta);

        inv.setItem(29, barrel);
        inv.setItem(30, gunCase);
        inv.setItem(31, cooler);
        inv.setItem(32, laser);
        inv.setItem(33, name);
        inv.setItem(40, gun);
        inv.setItem(49, shop);

        p.openInventory(inv);
    }

    @Override
    public void click(final Player p, int slot) {
        LaserPlayer lp = LaserPlayer.getPlayer(p.getName());
        final Gun g = lp.getGun();
        if (slot == 33) {
            AnvilGUI gui = new AnvilGUI(p, new AnvilGUI.AnvilClickEventHandler() {
                @Override
                public void onAnvilClick(AnvilGUI.AnvilClickEvent event) {
                    if (event.getSlot() == AnvilGUI.AnvilSlot.OUTPUT) {
                        event.setWillClose(true);
                        event.setWillDestroy(true);
                        g.setName(event.getName());

                    } else {
                        event.setWillClose(false);
                        event.setWillDestroy(false);
                    }
                }
            });

            gui.setSlot(AnvilGUI.AnvilSlot.INPUT_LEFT, ItemUtil.createItem(g.getGunCase().getMaterial(), g.getName()));

            gui.open();
        }
        if (slot == 49) {
            LaserQuestInventorys.getGunBuilderStore().open(p);
        }
    }
}
