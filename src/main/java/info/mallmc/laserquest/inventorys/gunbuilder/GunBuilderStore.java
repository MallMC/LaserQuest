package info.mallmc.laserquest.inventorys.gunbuilder;

import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.util.ItemUtil;
import info.mallmc.laserquest.game.guns.Barrel;
import info.mallmc.laserquest.game.guns.Case;
import info.mallmc.laserquest.game.guns.Cooler;
import info.mallmc.laserquest.game.guns.Laser;
import info.mallmc.laserquest.inventorys.LaserQuestInventorys;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

/**
 *
 * @author Rushmead
 */
public class GunBuilderStore extends MallInventory {

    @Override
    public String getName() {
        return "Gun Builder Store";
    }

    @Override
    public void open(Player p) {
        Inventory inv = Bukkit.createInventory(p, 54, getName());

        inv.setItem(28, ItemUtil.createItem(Barrel.DEFAULT.getItem().getType(), "&aBarrels"));
        inv.setItem(30, ItemUtil.createItem(Case.DEFAULT.getItem().getType(), "&aCases"));
        inv.setItem(32, ItemUtil.createItem(Cooler.DEFAULT.getItem().getType(), "&aCoolers"));
        inv.setItem(34, ItemUtil.createItem(Laser.DEFAULT.getItem().getType(), "&aLasers"));
        inv.setItem(49, ItemUtil.createItem(Material.ARROW, "&aBack"));
        p.openInventory(inv);
    }

    @Override
    public void click(Player p, int slot) {
        if (slot == 49) {
            p.closeInventory();
            LaserQuestInventorys.getGunBuilderMain().open(p);
        } else if (slot == 28) {
            p.closeInventory();
            LaserQuestInventorys.getGunBuilderBarrels().open(p);
        } else if (slot == 34) {
            p.closeInventory();
            LaserQuestInventorys.getGunBuilderLasers().open(p);
        } else if (slot == 30) {
            p.closeInventory();
            LaserQuestInventorys.getGunBuilderCases().open(p);
        } else if (slot == 32) {
            p.closeInventory();
            LaserQuestInventorys.getGunBuilderCoolers().open(p);
        }
    }
}
