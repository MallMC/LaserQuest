package info.mallmc.laserquest.inventorys.gunbuilder;

import info.mallmc.framework.api.MallInventory;
import info.mallmc.framework.api.MallPlayer;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.framework.util.ItemUtil;
import info.mallmc.laserquest.game.guns.Laser;
import info.mallmc.laserquest.inventorys.LaserQuestInventorys;
import info.mallmc.laserquest.player.LaserPlayer;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import java.util.Arrays;
import java.util.HashMap;

/**
 *
 * @author Rushmead
 */
public class GunBuilderLasers extends MallInventory {
    
    private HashMap<Integer, Laser> lasers = new HashMap<>();
    
    @Override
    public String getName() {
        return "Lasers Store";
    }
    
    @Override
    public void open(Player p) {
        Inventory inv = Bukkit.createInventory(p, 54, getName());
        LaserPlayer lp = LaserPlayer.getPlayer(p.getName());
        int i = 10;
        for (Laser b : Laser.values()) {
            if (b == Laser.NONE) {
                continue;
            }
            if (lp.hasLaser(b)) {
                ItemStack item = new ItemStack(b.getItem().getType(), 1, (byte) b.getItemColour().getData());
                ItemMeta im = item.getItemMeta();
                im.setDisplayName(Messaging.colorizeMessage("&a" + b.getName()));
                
                im.setLore(Arrays.asList(Messaging.colorizeMessage("&aClick to set as Barrel"), Messaging.colorizeMessage("&4Changes Colour of Laser")));
                item.setItemMeta(im);
                inv.setItem(i, item);
            } else {
                inv.setItem(i, ItemUtil.createItem(Material.WEB, "&4" + b.getName(), "&aCost: &6" + b.getCost(), "&aClick to buy"));
            }
            if (lp.getGun().getLaser() == b) {
                inv.getItem(i).addUnsafeEnchantment(Enchantment.LUCK, 1);
            }
            lasers.put(i, b);
            i++;
            if (i >= 16) {
                i = 19;
            } else if (i >= 25) {
                i = 28;
            } else if (i >= 34) {
                i = 37;
            }
        }
        inv.setItem(49, ItemUtil.createItem(Material.ARROW, "&aGo Back "));
        p.openInventory(inv);
    }
    
    @Override
    public void click(Player p, int slot) {
        MallPlayer ep = MallPlayer.getPlayer(p.getName());
        LaserPlayer lp = LaserPlayer.getPlayer(p.getName());
        if (slot == 49) {
            p.closeInventory();
            LaserQuestInventorys.getGunBuilderStore().open(p);
        } else if (lasers.containsKey(slot)) {
            Laser b = lasers.get(slot);
            if (p.getOpenInventory().getItem(slot).getType() == Material.WEB) {
                if (ep.getCurrency() >= b.getCost()) {
                    lp.unlockLaser(b);
                    ep.purchase(b.getCost());
                    p.closeInventory();
                    open(p);
                } else {
                    Messaging.sendMessage(p, "global.currency.notenough");
                }
            } else {
                lp.getGun().setLaser(b);
                p.closeInventory();
                open(p);
            }
        }
    }
}
