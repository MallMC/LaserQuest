package info.mallmc.laserquest.util;

import info.mallmc.framework.messaging.Messaging;
import java.util.ArrayList;
import java.util.List;

import info.mallmc.laserquest.player.LaserPlayer;
import info.mallmc.laserquest.teams.Team;
import info.mallmc.laserquest.LaserQuest;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.Objective;

/**
 *
 * @author Rushmead
 */
public class Scoreboard {

    private static org.bukkit.scoreboard.Scoreboard sb = Bukkit.getScoreboardManager().getNewScoreboard();
    private static Objective lobby = null;

    public static org.bukkit.scoreboard.Scoreboard getScoreboard() {
        return sb;
    }

    public static void lobby(Player player) {
        if (player == null) {
            return;
        }
        if (LaserPlayer.getPlayer(player.getName()) == null) {
            return;
        }
        if (SimpleScoreboard.getManager().getScoreboard(player) == null) {
            SimpleScoreboard.getManager().setupBoard(player);
        }
        if (SimpleScoreboard.getManager().getScoreboard(player) != null) {
            SimpleScoreboard.getManager().getScoreboard(player).unregister();
            SimpleScoreboard.getManager().setupBoard(player);
        }
        SimpleScoreboard.getManager().getScoreboard(player).setHeader(ChatColor.BLUE + "   " + Messaging.getPrefix() + "   ");
        SimpleScoreboard.getManager().getScoreboard(player).setScores(Messaging.colorizeMessage("&3Game:"), Messaging.colorizeMessage("&6Laser Quest"), Messaging.colorizeMessage("&3Overall Score:"), Messaging.colorizeMessage("&6&l" + LaserPlayer.getPlayer(player.getName()).getTotalPoints()), Messaging.colorizeMessage("&3Map:"), Messaging.colorizeMessage("&6&l" + LaserQuest.getInstance().getMapManager().getCurrentMap().getMapName()), Messaging.colorizeMessage("&3Players:"), Messaging.colorizeMessage("&6&l" + Bukkit.getOnlinePlayers().size() + "/12"));
        SimpleScoreboard.getManager().getScoreboard(player).update();
    }

    public static void game(Player player) {
        if (SimpleScoreboard.getManager().getScoreboard(player) != null) {
            SimpleScoreboard.getManager().getScoreboard(player).unregister();
        }
        SimpleScoreboard.getManager().setupBoard(player);
        SimpleScoreboard.getManager().getScoreboard(player).setHeader(ChatColor.BLUE + "   " + Messaging.getPrefix() + "   ");
        List<String> scores = new ArrayList<>();
        scores.add(Messaging.colorizeMessage("&4Game:"));
        scores.add(Messaging.colorizeMessage("&6Laser Quest"));
        scores.add(Messaging.colorizeMessage("&4Team:"));
        scores.add(LaserPlayer.getPlayer(player.getName()).getTeam().getColor() + LaserPlayer.getPlayer(player.getName()).getTeam().getName());
        scores.add(Messaging.colorizeMessage("&4&l&nPoints"));
        scores.add(Messaging.colorizeMessage("&5&lYou: &6" + LaserPlayer.getPlayer(player.getName()).getPoints()));
        for (Team t : LaserQuest.getInstance().getTeamManager().getTeams().values()) {
            scores.add(Messaging.colorizeMessage("&l" + t.getColor() + t.getName() + ": &6" + t.getPoints()));
        }
        SimpleScoreboard.getManager().getScoreboard(player).setScores(scores);
        SimpleScoreboard.getManager().getScoreboard(player).update();
    }
}
