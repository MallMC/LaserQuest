package info.mallmc.laserquest.util;


import info.mallmc.framework.util.ParticleEffect;
import net.minecraft.server.v1_10_R1.EnumParticle;
import net.minecraft.server.v1_10_R1.PacketPlayOutWorldParticles;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class ShapeUtil {

    public static void runLongHelix(Player player, String effect) {
        Location loc = player.getLocation();
        int radius = 2;
        for (double y = 0; y <= 50; y += 0.05) {
            double x = radius * Math.cos(y);
            double z = radius * Math.sin(y);
            PacketPlayOutWorldParticles packet = new PacketPlayOutWorldParticles(EnumParticle.valueOf(effect.toUpperCase()), false, (float) (loc.getX() + x), (float) (loc.getY() + y), (float) (loc.getZ() + z), 0, 0, 0, 0, 1);
            for (Player online : Bukkit.getOnlinePlayers()) {
                ((CraftPlayer) online).getHandle().playerConnection.sendPacket(packet);
            }
        }

    }

    public static void runLongHelix(Player player, ParticleEffect effect, Color color) {
        Location loc = player.getLocation();
        double radius = 0.05;
        for (double y = 0; y <= 3; y += 0.05) {
            double x = radius * Math.cos(y);
            double z = radius * Math.sin(y);
            ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(color), new Location(loc.getWorld(), (float) (loc.getX() + x), (float) (loc.getY() + y), (float) (loc.getZ() + z)), 200D);
        }

    }
}
