package info.mallmc.laserquest.maps;

import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class Map {

    private String mapName;
    private String authors;
    private Location spawnLocation1;
    private Location spawnLocation2;
    
    public Map(String mapName, String authors, Location location1, Location location2) {
        this.mapName = mapName;
        this.authors = authors;
        this.spawnLocation1 = location1;
        this.spawnLocation2 = location2;
    }

    /**
     * @return the mapName
     */
    public String getMapName() {
        return mapName;
    }

    /**
     * @param mapName the mapName to set
     */
    public void setMapName(String mapName) {
        this.mapName = mapName;
    }

    /**
     * @return the authors
     */
    public String getAuthors() {
        return authors;
    }

    /**
     * @param authors the authors to set
     */
    public void setAuthors(String authors) {
        this.authors = authors;
    }

    /**
     * @return the spawnLocation1
     */
    public Location getSpawnLocation1() {
        return spawnLocation1;
    }

    /**
     * @param spawnLocation1 the spawnLocation1 to set
     */
    public void setSpawnLocation1(Location spawnLocation1) {
        this.spawnLocation1 = spawnLocation1;
    }

    /**
     * @return the spawnLocation2
     */
    public Location getSpawnLocation2() {
        return spawnLocation2;
    }

    /**
     * @param spawnLocation2 the spawnLocation2 to set
     */
    public void setSpawnLocation2(Location spawnLocation2) {
        this.spawnLocation2 = spawnLocation2;
    }

    public void teleportPlayerToLocation1(Player p) {
        p.teleport(spawnLocation1);
    }

    public void teleportPlayerToLocation2(Player p) {
        p.teleport(spawnLocation2);
    }
}
