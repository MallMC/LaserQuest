package info.mallmc.laserquest.maps;

import com.mongodb.BasicDBObject;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import info.mallmc.framework.Framework;
import info.mallmc.framework.database.Database;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

/**
 *
 * @author Rushmead
 */
public class MapManager {

    private HashMap<String, Map> maps = new HashMap<String, Map>();
    private Map currentMap;

    public synchronized void loadMaps() {
        Database db = Framework.getInstance().getMongoDatabase();

        DBObject r = new BasicDBObject("game", "laserquest");
        DBCursor cursor = db.getMaps().find(r);
        while (cursor.hasNext()) {
            DBObject map = cursor.next();
            Map m = new Map((String) map.get("name"), (String) map.get("authors"), getDeserializedLocation((String) map.get("spawnLocation1")), getDeserializedLocation((String) map.get("spawnLocation2")));
            maps.put((String) map.get("name"), m);
        }
        pickMap();
    }

    public void pickMap() {
       
        List<Map> valuesList = new ArrayList<Map>(maps.values());
        int randomIndex = new Random().nextInt(valuesList.size());
        currentMap = valuesList.get(randomIndex);
    }

    public Map getCurrentMap() {
        return currentMap;
    }

    public Map getMapByName(String name) {
        return maps.get(name);
    }

    public HashMap<String, Map> getMaps() {
        return maps;
    }

    public String getSerializedLocation(Location loc) { //Converts location -> String
        return loc.getX() + ";" + loc.getY() + ";" + loc.getZ();
        //feel free to use something to split them other than semicolons (Don't use periods or numbers)
    }

    public Location getDeserializedLocation(String s) {//Converts String -> Location
        String[] parts = s.split(";"); //If you changed the semicolon you must change it here too
        double x = Double.parseDouble(parts[0]);
        double y = Double.parseDouble(parts[1]);
        double z = Double.parseDouble(parts[2]);
        World w = Bukkit.getServer().getWorlds().get(0);
        return new Location(w, x, y, z); //can return null if the world no longer exists
    }
}
