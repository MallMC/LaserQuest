package info.mallmc.laserquest.commands;

import info.mallmc.framework.api.MallPlayer;
import info.mallmc.framework.api.PermissionSet;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.laserquest.LaserQuest;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class GameCommand implements CommandExecutor {

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
        if (sender instanceof Player && MallPlayer.getPlayer(((Player) sender).getName()).getPermissions().getPower() < PermissionSet.SNR_STAFF.getPower()) {
            Messaging.sendMessage(sender, "global.command.notallowed");
            return false;
        }
        if (args.length == 0) {
            Messaging.sendMessage(sender, "global.command.incorrectusage", "/game start/stop/restart");
            return false;
        }
        String subcommand = args[0];

        switch (subcommand) {
            case "start":
                LaserQuest.getInstance().getLobbyManager().startCountdown();
                break;
            case "force":
                LaserQuest.getInstance().getGameManager().prep();
                break;
            case "stop":
                LaserQuest.getInstance().getGameManager().stop();
                break;
        }

        return true;
    }

}
