package info.mallmc.laserquest.player;

import info.mallmc.laserquest.game.GameRank;
import info.mallmc.laserquest.game.guns.Case;
import info.mallmc.laserquest.teams.Team;
import info.mallmc.laserquest.game.guns.Barrel;
import info.mallmc.laserquest.game.guns.Cooler;
import info.mallmc.laserquest.game.guns.Gun;
import info.mallmc.laserquest.game.guns.Laser;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Rushmead
 */
public class LaserPlayer {

    private static LaserPlayerDB lpDB = new LaserPlayerDB();
    private static HashMap<String, LaserPlayer> players = new HashMap<>();

    public static HashMap<String, LaserPlayer> getPlayers() {
        return players;
    }

    public static LaserPlayer getPlayer(String name) {
        if (!players.containsKey(name)) {
            return null;
        }
        return players.get(name);
    }

    public static LaserPlayerDB getLaserDB() {
        return lpDB;
    }

    public static LaserPlayer createPlayer(Player p) {
        if (!players.containsKey(p.getDisplayName())) {
            LaserPlayer player = lpDB.getPlayer(p);
            if (player == null && p != null) {
                player = new LaserPlayer(p.getDisplayName());
            }
            players.put(p.getDisplayName(), player);
            return player;
        } else {
            return getPlayer(p.getDisplayName());
        }

    }

    public static void removePlayer(String name) {
        if (players.containsKey(name)) {
            players.remove(name);

        }
    }

    private String realName;
    // Per Instance Stuff
    private int points;
    private Team team;
    private boolean deactivated;
    private int deactivateTime;
    private int coolDown;
//Database Stuff
    private Gun gun;
    private int totalPoints;
    private List<Barrel> unlockedBarrels;
    private List<Laser> unlockedLasers;
    private List<Case> unlockedCases;
    private List<Cooler> unlockedCoolers;
    private GameRank gameRank;
    private int shots;
    private int deaths;
    private int games;
    private int wins;

    public LaserPlayer(String name) {
        this.realName = name;
        this.points = 0;
        this.team = null;
        this.deactivated = false;
        this.deactivateTime = 0;
        this.gun = new Gun("Basic Shooter", Barrel.DEFAULT, Case.DEFAULT, Cooler.DEFAULT, Laser.DEFAULT);
        this.totalPoints = 0;
        this.coolDown = 0;
        this.unlockedBarrels = new ArrayList<>();
        unlockedBarrels.add(Barrel.DEFAULT);
        this.unlockedLasers = new ArrayList<>();
        unlockedLasers.add(Laser.DEFAULT);
        this.unlockedCases = new ArrayList<>();
        unlockedCases.add(Case.DEFAULT);
        this.unlockedCoolers = new ArrayList<>();
        unlockedCoolers.add(Cooler.DEFAULT);
        this.deaths = 0;
        this.gameRank = GameRank.PRIVATE;
        this.shots = 0;
        this.games = 0;
        this.wins = 0;
    }

    /**
     *
     * @param name
     * @param uuid
     * @param g
     * @param totalPoints
     * @param barrels
     * @param lasers
     * @param cases
     * @param coolers
     * @param gameRank
     * @param shots
     * @param deaths
     * @param games
     * @param wins
     */
    public LaserPlayer(String name, String uuid, Gun g, int totalPoints, ArrayList<Integer> barrels, ArrayList<Integer> lasers, ArrayList<Integer> cases, ArrayList<Integer> coolers, GameRank gameRank, int shots, int deaths, int games, int wins) {
        this.realName = name;
        this.points = 0;
        this.team = null;
        this.deactivated = false;
        this.deactivateTime = 0;
        this.gun = g;
        this.coolDown = 0;
        this.totalPoints = totalPoints;
        this.unlockedBarrels = getUnlockedBarrels(barrels);
        this.unlockedCases = getUnlockedCases(cases);
        this.unlockedCoolers = getUnlockedCoolers(coolers);
        this.unlockedLasers = getUnlockedLasers(lasers);
        this.deaths = deaths;
        this.gameRank = gameRank;
        this.shots = shots;
        this.games = games;
        this.wins = wins;
    }

    public Player getPlayer() {
        return Bukkit.getPlayer(getRealName());
    }

    /**
     * @return the realName
     */
    public String getRealName() {
        return realName;
    }

    public int getGames() {
        return games;
    }

    public void setGames(int games) {
        this.games = games;
    }

    public int getWins() {
        return wins;
    }

    public void setWins(int wins) {
        this.wins = wins;
    }

    /**
     * @return the points
     */
    public int getPoints() {
        return points;
    }

    /**
     * @param points the points to set
     */
    public void setPoints(int points) {
        this.points = points;
    }

    /**
     * @return the team
     */
    public Team getTeam() {
        return team;
    }

    /**
     * @param team the team to set
     */
    public void setTeam(Team team) {
        this.team = team;
    }

    /**
     * @return the deactivated
     */
    public boolean isDeactivated() {
        return deactivated;
    }

    /**
     * @param deactivated the deactivated to set
     */
    public void setDeactivated(boolean deactivated) {
        this.deactivated = deactivated;
    }

    /**
     * @return the gun
     */
    public Gun getGun() {
        return gun;
    }

    /**
     * @param gun the gun to set
     */
    public void setGun(Gun gun) {
        this.gun = gun;
    }

    /**
     * @return the totalPoints
     */
    public int getTotalPoints() {
        return totalPoints;
    }

    /**
     * @param totalPoints the totalPoints to set
     */
    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }

    /**
     * @return the deactivateTime
     */
    public int getDeactivateTime() {
        return deactivateTime;
    }

    /**
     * @param deactivateTime the deactivateTime to set
     */
    public void setDeactivateTime(int deactivateTime) {
        this.deactivateTime = deactivateTime;
    }

    /**
     * @return the coolDown
     */
    public int getCoolDown() {
        return coolDown;
    }

    /**
     * @param coolDown the coolDown to set
     */
    public void setCoolDown(int coolDown) {
        this.coolDown = coolDown;
    }

    public static List<Barrel> getUnlockedBarrels(List<Integer> list) {
        List<Barrel> unlockedToys = new ArrayList<>();
        HashMap<Integer, Barrel> allBarrels = new HashMap<>();

        for (Barrel toy : Barrel.values()) {
            allBarrels.put(toy.getId(), toy);
        }

        for (int barrelID : list) {
            unlockedToys.add(allBarrels.get(barrelID));
        }

        return unlockedToys;
    }

    public static List<Laser> getUnlockedLasers(List<Integer> list) {
        List<Laser> unlockedToys = new ArrayList<>();
        HashMap<Integer, Laser> allBarrels = new HashMap<>();

        for (Laser toy : Laser.values()) {
            allBarrels.put(toy.getId(), toy);
        }

        for (int toyStr : list) {
            unlockedToys.add(allBarrels.get(toyStr));
        }

        return unlockedToys;
    }

    public static List<Case> getUnlockedCases(List<Integer> list) {
        List<Case> unlockedToys = new ArrayList<>();
        HashMap<Integer, Case> allBarrels = new HashMap<>();

        for (Case toy : Case.values()) {
            allBarrels.put(toy.getId(), toy);
        }

        for (int toyStr : list) {
            unlockedToys.add(allBarrels.get(toyStr));
        }

        return unlockedToys;
    }

    public static List<Cooler> getUnlockedCoolers(List<Integer> list) {
        List<Cooler> unlockedToys = new ArrayList<>();
        HashMap<Integer, Cooler> allBarrels = new HashMap<>();

        for (Cooler toy : Cooler.values()) {
            allBarrels.put(toy.getId(), toy);
        }

        for (int toyStr : list) {
            unlockedToys.add(allBarrels.get(toyStr));
        }

        return unlockedToys;
    }

    public void unlockBarrel(Barrel b) {
        if (!unlockedBarrels.contains(b)) {
            unlockedBarrels.add(b);
        }
    }

    public void unlockLaser(Laser b) {
        if (!unlockedLasers.contains(b)) {
            unlockedLasers.add(b);
        }
    }

    public void unlockCooler(Cooler b) {
        if (!unlockedCoolers.contains(b)) {
            unlockedCoolers.add(b);
        }
    }

    public void unlockCase(Case b) {
        if (!unlockedCases.contains(b)) {
            unlockedCases.add(b);
        }
    }

    /**
     * @return the unlockedBarrels
     */
    public List<Barrel> getUnlockedBarrels() {
        return unlockedBarrels;
    }

    /**
     * @return the unlockedLasers
     */
    public List<Laser> getUnlockedLasers() {
        return unlockedLasers;
    }

    /**
     * @return the unlockedCases
     */
    public List<Case> getUnlockedCases() {
        return unlockedCases;
    }

    /**
     * @return the unlockedCoolers
     */
    public List<Cooler> getUnlockedCoolers() {
        return unlockedCoolers;
    }

    public boolean hasBarrel(Barrel b) {
        return unlockedBarrels.contains(b);
    }

    public boolean hasLaser(Laser b) {
        return unlockedLasers.contains(b);
    }

    public boolean hasCases(Case b) {
        return unlockedCases.contains(b);
    }

    public boolean hasCooler(Cooler b) {
        return unlockedCoolers.contains(b);
    }

    public ArrayList<Integer> getUnlockedBarrelsString() {
        ArrayList<Integer> unlocked = new ArrayList<>();
        for (Barrel toy : unlockedBarrels) {
            if (toy.getId() != 0) {
                unlocked.add(toy.getId());
            }
        }

        return unlocked;
    }

    public ArrayList<Integer> getUnlockedLasersString() {
        ArrayList<Integer> unlocked = new ArrayList<>();
        for (Laser toy : unlockedLasers) {
            if (toy.getId() != 0) {
                unlocked.add(toy.getId());
            }
        }

        return unlocked;
    }

    public ArrayList<Integer> getUnlockedCasesString() {
        ArrayList<Integer> unlocked = new ArrayList<>();
        for (Case toy : unlockedCases) {
            if (toy.getId() != 0) {
                unlocked.add(toy.getId());
            }
        }

        return unlocked;
    }

    public ArrayList<Integer> getUnlockedCoolersString() {
        ArrayList<Integer> unlocked = new ArrayList<>();
        for (Cooler toy : unlockedCoolers) {
            if (toy.getId() != 0) {
                unlocked.add(toy.getId());
            }
        }

        return unlocked;
    }

    public GameRank getGameRank() {
        return gameRank;
    }

    public void setGameRank(GameRank gameRank) {
        this.gameRank = gameRank;
    }

    public int getShots() {
        return shots;
    }

    public void setShots(int shots) {
        this.shots = shots;
    }

    public int getDeaths() {
        return deaths;
    }

    public void setDeaths(int deaths) {
        this.deaths = deaths;
    }
}
