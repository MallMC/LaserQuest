package info.mallmc.laserquest.player;

import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;
import info.mallmc.framework.Framework;
import info.mallmc.framework.database.Database;
import info.mallmc.laserquest.game.GameRank;
import info.mallmc.laserquest.game.guns.*;
import org.bukkit.entity.Player;

import java.util.ArrayList;

/**
 *
 * @author Rushmead
 */
public class LaserPlayerDB {

    private final Database database = Framework.getInstance().getMongoDatabase();

    /**
     * Add a player (with default laser player data) to the database
     *
     * @param uuid The UUID of the player (in string form)
     */
    public synchronized void addPlayerToDatabase(String uuid) {
        if (isInDatabase(uuid)) {
            return;
        }
        ArrayList<Integer> barrels = new ArrayList<>();
        ArrayList<Integer> lasers = new ArrayList<>();
        ArrayList<Integer> cases = new ArrayList<>();
        ArrayList<Integer> coolers = new ArrayList<>();
        barrels.add(1);
        lasers.add(1);
        cases.add(1);
        coolers.add(1);
        DBObject obj = new BasicDBObject("uuid", uuid);
        obj.put("gun", ""); //TODO OBJECT
        obj.put("totalPoints", (int) 0);
        obj.put("unlockedBarrels", barrels);
        obj.put("unlockedLasers", lasers);
        obj.put("unlockedCases", cases);
        obj.put("unlockedCoolers", coolers);
        obj.put("gameRank", (int) 0);
        obj.put("shots", (int) 0);
        obj.put("deaths", (int) 0);
        obj.put("games", (int) 0);
        obj.put("wins", (int) 0);

        database.getLaserPlayers().save(obj);
    }

    /**
     * Check if a UUID is already in the database
     *
     * @param uuid The player UUID (in string form)
     *
     * @return Is the player in the database?
     */
    public synchronized boolean isInDatabase(String uuid) {
        DBObject r = new BasicDBObject("uuid", uuid);

        DBObject found = database.getLaserPlayers().findOne(r);

        if (found == null) {
            return false;
        }
        return true;
    }

    /**
     * Return an Laser Player from a Player's name
     *
     * @param p The player
     *
     * @return The Hub player
     */
    public synchronized LaserPlayer getPlayer(Player p) {
        LaserPlayer player = null;
        try {
            DBObject r = new BasicDBObject("uuid", p.getUniqueId().toString());
            DBObject foundUser = database.getLaserPlayers().findOne(r);
            if (foundUser != null) {
                Gson gson = new Gson();
                Gun g;
                if (foundUser.get("gun").toString().isEmpty() || ((BasicDBObject) foundUser.get("gun")).isEmpty()) {
                    g = new Gun("Basic Shooter", Barrel.DEFAULT, Case.DEFAULT, Cooler.DEFAULT, Laser.DEFAULT);
                } else {

                    g = gson.fromJson(((BasicDBObject) foundUser.get("gun")).toJson(), Gun.class);
                }
                player = new LaserPlayer(p.getName(), p.getUniqueId().toString(), g, (int) foundUser.get("totalPoints"), (ArrayList<Integer>) foundUser.get("unlockedBarrels"), (ArrayList<Integer>) foundUser.get("unlockedLasers"), (ArrayList<Integer>) foundUser.get("unlockedCases"), (ArrayList<Integer>) foundUser.get("unlockedCoolers"), GameRank.getRankFromID((int) foundUser.get("gameRank")), (int) foundUser.get("shots"), (int) foundUser.get("deaths"), (int) foundUser.get("wins"), (int) foundUser.get("games"));
                return player;

            } else {
                throw new IndexOutOfBoundsException();
            }

        } catch (IndexOutOfBoundsException ex) {
            addPlayerToDatabase(p.getUniqueId().toString());
            player = new LaserPlayer(p.getDisplayName());
        }

        return player;
    }

    /**
     * Save a player's information to the database
     *
     * @param player The player you are saving to the database
     */
    public synchronized void savePlayerToDB(LaserPlayer player) {
        DBObject r = new BasicDBObject("uuid", player.getPlayer().getUniqueId().toString());
        DBObject foundUser = database.getLaserPlayers().findOne(r);
        if (foundUser == null) {
            return;
        }
        foundUser.removeField("gun");
        foundUser.removeField("totalPoints");
        foundUser.removeField("unlockedBarrels");
        foundUser.removeField("unlockedLasers");
        foundUser.removeField("unlockedCases");
        foundUser.removeField("unlockedCoolers");
        foundUser.removeField("gameRank");
        foundUser.removeField("shots");
        foundUser.removeField("deaths");
        foundUser.removeField("games");
        foundUser.removeField("wins");
        Gson gson = new Gson();

        foundUser.put("gun", JSON.parse(gson.toJson(player.getGun(), Gun.class)));
        foundUser.put("totalPoints", player.getTotalPoints());
        foundUser.put("unlockedBarrels", player.getUnlockedBarrelsString());
        foundUser.put("unlockedLasers", player.getUnlockedLasersString());
        foundUser.put("unlockedCases", player.getUnlockedCasesString());
        foundUser.put("unlockedCoolers", player.getUnlockedCoolersString());
        foundUser.put("gameRank", player.getGameRank().getId());
        foundUser.put("shots", player.getShots());
        foundUser.put("deaths", player.getDeaths());
        foundUser.put("games", player.getGames());
        foundUser.put("wins", player.getWins());
        database.getLaserPlayers().save(foundUser);
    }
}
