package info.mallmc.laserquest.lobby;

import info.mallmc.framework.items.FrameworkItems;
import info.mallmc.framework.util.ItemUtil;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Rushmead
 */
public class LobbyItems {

    public static ItemStack[] getInventory() {
        ItemStack[] items = new ItemStack[9];
        items[0] = ItemUtil.createItem(Material.BOOK, "&6Choose Team");
        items[1] = ItemUtil.createItem(Material.EMERALD, "&6Gun Builder");
        items[8] = FrameworkItems.backToHub();
        return items;
    }
}
