package info.mallmc.laserquest.teams;

import java.util.HashMap;

import info.mallmc.laserquest.util.Scoreboard;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class TeamManager {

    private HashMap<String, Team> teams = new HashMap<String, Team>();
    private HashMap<Team, org.bukkit.scoreboard.Team> scoreboardTeams = new HashMap<Team, org.bukkit.scoreboard.Team>();
    private HashMap<String, Team> playerTeams = new HashMap<>();

    public HashMap<String, Team> getTeams() {
        return teams;
    }

    public HashMap<String, Team> getPlayerTeams() {
        return playerTeams;
    }

    public HashMap<Team, org.bukkit.scoreboard.Team> getScoreboardTeams() {
        return scoreboardTeams;
    }

    public Team getTeamFromPlayer(Player p) {
        return playerTeams.get(p.getName());
    }

    public Team getTeamByName(String name) {
        return teams.get(name);
    }

    public void registerTeam(String name, Team team) {
        teams.put(name, team);
        org.bukkit.scoreboard.Team T = Scoreboard.getScoreboard().registerNewTeam(name);
        T.setDisplayName(team.getColor() + name);
        T.setPrefix(team.getColor() + "[" + name + "]");
        scoreboardTeams.put(team, T);
    }

    public boolean hasTeam(Player p) {
        return playerTeams.containsKey(p.getName());
    }

    public boolean inTeam(Team team, Player p) {
        if (playerTeams.containsKey(p) && playerTeams.get(p) == team) {
            return true;
        } else {
            return false;
        }
    }

}
