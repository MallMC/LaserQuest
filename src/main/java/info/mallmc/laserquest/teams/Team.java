package info.mallmc.laserquest.teams;

import java.util.ArrayList;
import java.util.List;
import info.mallmc.laserquest.LaserQuest;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.entity.Player;

/**
 *
 * @author Rushmead
 */
public class Team {

    private List<String> players = new ArrayList<String>();
    private String teamName;
    private ChatColor color;
    private int points = 0;

    public Team(String name, ChatColor teamColor) {
        teamName = name;
        color = teamColor;
        LaserQuest.getInstance().getTeamManager().registerTeam(teamName, this);

    }

    public String getName() {
        return teamName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int i) {
        points = i;
    }

    public void addPoint() {
        points++;
    }

    public void addPlayer(Player p) {
        players.add(p.getName());
        LaserQuest.getInstance().getTeamManager().getPlayerTeams().put(p.getName(), this);
    }

    public void removePlayer(Player p) {
        if (LaserQuest.getInstance().getTeamManager().hasTeam(p) && LaserQuest.getInstance().getTeamManager().inTeam(this, p)) {
            players.remove(p.getName());
            LaserQuest.getInstance().getTeamManager().getPlayerTeams().remove(p.getName());
        }
    }

    public void teleportTeam(Location l) {
        for (String name : players) {
            Player p = Bukkit.getPlayer(name);
            p.teleport(l);
        }
    }

    /**
     * @return the color
     */
    public ChatColor getColor() {
        return color;
    }
}
