package info.mallmc.laserquest;


import info.mallmc.laserquest.events.FrameworkEnable;
import info.mallmc.laserquest.events.ProjectileHit;
import info.mallmc.laserquest.events.ResourcePack;
import info.mallmc.laserquest.events.player.PlayerInteract;
import info.mallmc.laserquest.events.player.PlayerJoin;
import info.mallmc.laserquest.events.player.PlayerMovement;
import info.mallmc.laserquest.game.GameManager;
import info.mallmc.laserquest.lobby.LobbyManager;
import info.mallmc.laserquest.maps.MapManager;
import info.mallmc.laserquest.teams.Team;
import info.mallmc.laserquest.events.InventoryClick;
import info.mallmc.laserquest.events.OneMinute;
import info.mallmc.laserquest.events.OneSecond;
import info.mallmc.laserquest.events.player.PickUpItem;
import info.mallmc.laserquest.events.player.PlayerDamage;
import info.mallmc.laserquest.events.player.PlayerLeave;
import info.mallmc.laserquest.teams.TeamManager;
import info.mallmc.laserquest.util.SimpleScoreboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.java.JavaPlugin;

/**
 *
 * @author Rushmead
 */
public class LaserQuest extends JavaPlugin {

    private static LaserQuest instance;
    private TeamManager teamManager;
    private GameManager gameManager;
    private LobbyManager lobbyManager;
    private MapManager mapManager;

    public static LaserQuest getInstance() {
        return instance;
    }

    public void onEnable() {
        instance = this;
        PluginManager pm = Bukkit.getPluginManager();
        teamManager = new TeamManager();
        gameManager = new GameManager();
        lobbyManager = new LobbyManager();
        mapManager = new MapManager();

        pm.registerEvents(new FrameworkEnable(), this);
        pm.registerEvents(new OneMinute(), this);
        pm.registerEvents(new OneSecond(), this);
        pm.registerEvents(new PlayerJoin(), this);
        pm.registerEvents(new InventoryClick(), this);
        pm.registerEvents(new PlayerInteract(), this);
        pm.registerEvents(new ResourcePack(), this);
        pm.registerEvents(new PlayerDamage(), this);
        pm.registerEvents(new PickUpItem(), this);
        pm.registerEvents(new ProjectileHit(), this);
        pm.registerEvents(new PlayerMovement(), this);
        pm.registerEvents(new PlayerLeave(), this);
        new Team("Red", ChatColor.RED);
        new Team("Green", ChatColor.GREEN);
        mapManager.loadMaps();
        SimpleScoreboard.getManager().start();
    }

    public TeamManager getTeamManager() {
        return teamManager;
    }

    public GameManager getGameManager() {
        return gameManager;
    }

    public LobbyManager getLobbyManager() {
        return lobbyManager;
    }

    public MapManager getMapManager() {
        return mapManager;
    }

}
