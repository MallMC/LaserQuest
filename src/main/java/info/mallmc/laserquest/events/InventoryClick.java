package info.mallmc.laserquest.events;

import info.mallmc.laserquest.inventorys.LaserQuestInventorys;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;

/**
 *
 * @author Rushmead
 */
public class InventoryClick implements Listener {

    @EventHandler
    public void onClick(InventoryClickEvent e) {
        e.setCancelled(true);
        if (ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase(LaserQuestInventorys.getTeamSelector().getName())) {

            LaserQuestInventorys.getTeamSelector().click((Player) e.getWhoClicked(), e.getSlot());
        }
        if (ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase(LaserQuestInventorys.getGunBuilderMain().getName())) {
            LaserQuestInventorys.getGunBuilderMain().click((Player) e.getWhoClicked(), e.getSlot());
        }
        if (ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase(LaserQuestInventorys.getGunBuilderStore().getName())) {
            LaserQuestInventorys.getGunBuilderStore().click((Player) e.getWhoClicked(), e.getSlot());
        }
        if (ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase(LaserQuestInventorys.getGunBuilderBarrels().getName())) {
            LaserQuestInventorys.getGunBuilderBarrels().click((Player) e.getWhoClicked(), e.getSlot());
        }
        if (ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase(LaserQuestInventorys.getGunBuilderLasers().getName())) {
            LaserQuestInventorys.getGunBuilderLasers().click((Player) e.getWhoClicked(), e.getSlot());
        }
        if (ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase(LaserQuestInventorys.getGunBuilderCases().getName())) {
            LaserQuestInventorys.getGunBuilderCases().click((Player) e.getWhoClicked(), e.getSlot());
        }
        if (ChatColor.stripColor(e.getClickedInventory().getTitle()).equalsIgnoreCase(LaserQuestInventorys.getGunBuilderCoolers().getName())) {
            LaserQuestInventorys.getGunBuilderCoolers().click((Player) e.getWhoClicked(), e.getSlot());
        }
    }
}
