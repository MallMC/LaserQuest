package info.mallmc.laserquest.events;

import info.mallmc.framework.messaging.Messaging;
import info.mallmc.laserquest.util.InstantFirework;
import info.mallmc.laserquest.LaserQuest;
import info.mallmc.laserquest.player.LaserPlayer;
import info.mallmc.laserquest.util.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.FireworkEffect;
import org.bukkit.FireworkEffect.Type;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;

/**
 * @author Rushmead
 */
public class ProjectileHit implements Listener {

    @EventHandler
    public void onEvent(EntityDamageByEntityEvent e) {
        if (e.getDamager() instanceof Arrow) {
            Arrow damager = (Arrow) e.getDamager();
            if (e.getEntity() instanceof Player) {
                Player p = (Player) e.getEntity();
                LaserPlayer lp = LaserPlayer.getPlayer(p.getName());
                Player shooter = (Player) damager.getShooter();
                LaserPlayer lpS = LaserPlayer.getPlayer(shooter.getName());
                if (lp.isDeactivated()) {
                    Messaging.sendMessage(shooter, "laserquest.game.general.playerDeactivated");
                    return;
                }
                if (lp.getTeam() == lpS.getTeam()) {
                    Messaging.sendMessage(shooter, "laserquest.game.general.playerTeam");
                    return;
                }
                double projectile_height = e.getEntity().getLocation().getY();
                double player_bodyheight = 1.35;
                if (projectile_height > player_bodyheight) {
                    if (lpS.getGun().getLaser().getColor() == Color.WHITE) {
                        if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getName() == "Red") {
                            FireworkEffect fireworkEffect = FireworkEffect.builder().flicker(false).trail(true).with(Type.BURST).withColor(Color.GREEN).build();
                            Location location = p.getLocation();
                            new InstantFirework(fireworkEffect, location);
                        } else {
                            FireworkEffect fireworkEffect = FireworkEffect.builder().flicker(false).trail(true).with(Type.BURST).withColor(Color.RED).build();
                            Location location = p.getLocation();
                            new InstantFirework(fireworkEffect, location);
                        }
                    } else {
                        FireworkEffect fireworkEffect = FireworkEffect.builder().flicker(false).trail(true).with(Type.BURST).withColor(lpS.getGun().getLaser().getColor()).build();
                        Location location = p.getLocation();
                        new InstantFirework(fireworkEffect, location);
                    }

                    Messaging.sendMessage(p, "laserquest.game.general.shot", lpS.getGun().getBarrel().getDamage() * 2, shooter.getName());
                    Messaging.sendMessage(shooter, "laserquest.game.general.hit", p.getName());
                    if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getName() == "Red") {
                        LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation1(p);
                    } else {
                        LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation2(p);
                    }
                    for (Player ps : Bukkit.getOnlinePlayers()) {
                        ps.playSound(ps.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);

                    }

                    lp.setDeactivated(true);
                    lp.setDeactivateTime(lpS.getGun().getBarrel().getDamage() * 2);
                    lp.getPlayer().setLevel(lp.getDeactivateTime());
                    lp.setDeaths(lp.getDeaths() + 1);
                    lpS.setPoints(lpS.getPoints() + 1);

                    lpS.setTotalPoints(lpS.getTotalPoints() + 1);
                    lpS.getTeam().addPoint();
                    for (Player ps : Bukkit.getOnlinePlayers()) {
                        Scoreboard.game(ps);
                    }

                }
            }
            e.getDamager().remove();
        }
    }
}
