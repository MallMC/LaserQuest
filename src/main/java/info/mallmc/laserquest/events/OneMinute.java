package info.mallmc.laserquest.events;

import info.mallmc.framework.api.GameState;
import info.mallmc.framework.customevents.OneMinuteEvent;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.laserquest.LaserQuest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Rushmead
 */
public class OneMinute implements Listener {

    @EventHandler
    public void onMinute(OneMinuteEvent e) {
        if (GameState.getGameState() == GameState.IN_GAME) {
            if (LaserQuest.getInstance().getGameManager().gameTime == 1 && !LaserQuest.getInstance().getGameManager().countdown) {
                LaserQuest.getInstance().getGameManager().countDownTime = 60;
                LaserQuest.getInstance().getGameManager().countdown = true;
            } else if (LaserQuest.getInstance().getGameManager().gameTime == 0) {
                LaserQuest.getInstance().getGameManager().stop();
                LaserQuest.getInstance().getGameManager().countdown = false;
            } else {
                Messaging.broadcastMessage("laserquest.game.timer.minutesLeft", LaserQuest.getInstance().getGameManager().gameTime);
                LaserQuest.getInstance().getGameManager().gameTime -= 1;
            }
        }
    }
}
