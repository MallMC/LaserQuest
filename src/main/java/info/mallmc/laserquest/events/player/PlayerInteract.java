package info.mallmc.laserquest.events.player;

import info.mallmc.framework.api.GameState;
import info.mallmc.framework.api.MallPlayer;
import info.mallmc.framework.api.PermissionSet;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.framework.util.ParticleEffect;
import info.mallmc.laserquest.LaserQuest;
import info.mallmc.laserquest.game.guns.Gun;
import info.mallmc.laserquest.inventorys.LaserQuestInventorys;
import info.mallmc.laserquest.player.LaserPlayer;
import info.mallmc.laserquest.util.InstantFirework;
import info.mallmc.laserquest.util.Scoreboard;
import net.md_5.bungee.api.ChatColor;
import org.bukkit.*;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.util.Vector;

/**
 * @author Rushmead
 */
public class PlayerInteract implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent e) {
        Player player = (Player) e.getPlayer();

        if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (MallPlayer.getPlayer(player.getName()).getPermissions().getPower() > PermissionSet.JNR_STAFF.getPower()) {
                e.setCancelled(false);
            } else {
                e.setCancelled(true);
            }
            if (e.getItem() != null && e.getItem().hasItemMeta() && e.getItem().getItemMeta().hasDisplayName()) {
                if (ChatColor.stripColor(e.getItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Choose Team")) {
                    LaserQuestInventorys.getTeamSelector().open(player);
                }
                if (ChatColor.stripColor(e.getItem().getItemMeta().getDisplayName()).equalsIgnoreCase("Gun Builder")) {
                    LaserQuestInventorys.getGunBuilderMain().open(player);
                }
                if (GameState.getGameState() == GameState.IN_GAME) {
                    if (LaserPlayer.getPlayer(player.getName()).isDeactivated()) {
                        Messaging.sendMessage(player, "laserquest.game.general.deactivated");
                        return;
                    }
                    if (LaserPlayer.getPlayer(player.getName()).getCoolDown() > 0) {
                        Messaging.sendMessage(player, "laserquest.game.general.cooldown", LaserPlayer.getPlayer(player.getName()).getCoolDown());
                        return;
                    }
                    Gun gt = LaserPlayer.getPlayer(player.getName()).getGun();
                    if (gt == null) {
                        return;
                    }
                    if (e.getItem().getType() == gt.getGunCase().getMaterial()) {

                        Location start = player.getEyeLocation();
                        Vector increase = start.getDirection();
                        for (int counter = 0; counter < 100; counter++) {
                            Location point = start.add(increase);
                            if (gt.getLaser().getColor() == Color.WHITE) {
                                if (LaserPlayer.getPlayer(e.getPlayer().getName()).getTeam().getName() == "Red") {
                                    ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(Color.RED), point, 200D);
                                } else {
                                    ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(Color.GREEN), point, 200D);
                                }
                            } else {
                                ParticleEffect.REDSTONE.display(new ParticleEffect.OrdinaryColor(gt.getLaser().getColor()), point, 200D);
                            }
                            if (counter > 5) {
                                for (Entity entity : point.getWorld().getNearbyEntities(point, 1, 1, 1)) {
                                    if (entity instanceof Player) {
                                        Player p = (Player) entity;
                                        LaserPlayer lp = LaserPlayer.getPlayer(p.getName());
                                        Player shooter = (Player) player;
                                        LaserPlayer lpS = LaserPlayer.getPlayer(shooter.getName());
                                        if (lp.isDeactivated()) {
                                            Messaging.sendMessage(shooter, "laserquest.game.general.playerDeactivated");
                                            return;
                                        }
                                        if (lp.getTeam() == lpS.getTeam()) {
                                            Messaging.sendMessage(shooter, "laserquest.game.general.playerTeam");
                                            return;
                                        }
                                        double projectile_height = entity.getLocation().getY();
                                        double player_bodyheight = 1.35;
                                        if (projectile_height > player_bodyheight) {
                                            if (lpS.getGun().getLaser().getColor() == Color.WHITE) {
                                                if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getName().equals("Red")) {
                                                    FireworkEffect fireworkEffect = FireworkEffect.builder().flicker(false).trail(true).with(FireworkEffect.Type.BURST).withColor(Color.GREEN).build();
                                                    Location location = p.getLocation();
                                                    InstantFirework firework = new InstantFirework(fireworkEffect, location);

                                                } else {
                                                    FireworkEffect fireworkEffect = FireworkEffect.builder().flicker(false).trail(true).with(FireworkEffect.Type.BURST).withColor(Color.RED).build();
                                                    Location location = p.getLocation();
                                                    InstantFirework firework = new InstantFirework(fireworkEffect, location);
                                                }
                                            } else {
                                                FireworkEffect fireworkEffect = FireworkEffect.builder().flicker(false).trail(true).with(FireworkEffect.Type.BURST).withColor(lpS.getGun().getLaser().getColor()).build();
                                                Location location = p.getLocation();
                                                InstantFirework firework = new InstantFirework(fireworkEffect, location);
                                            }

                                            Messaging.sendMessage(p, "laserquest.game.general.shot", lpS.getGun().getBarrel().getDamage() * 2, shooter.getName());
                                            Messaging.sendMessage(shooter, "laserquest.game.general.hit", p.getName());
                                            if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getName().equals("Red")) {
                                                LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation1(p);
                                            } else {
                                                LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation2(p);
                                            }
                                            for (Player ps : Bukkit.getOnlinePlayers()) {
                                                ps.playSound(ps.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);
                                            }

                                            lp.setDeactivated(true);
                                            lp.setDeactivateTime(lpS.getGun().getBarrel().getDamage() * 2);
                                            lp.getPlayer().setLevel(lp.getDeactivateTime());
                                            lp.setDeaths(lp.getDeaths() + 1);
                                            lpS.setPoints(lpS.getPoints() + 1);

                                            lpS.setTotalPoints(lpS.getTotalPoints() + 1);
                                            lpS.getTeam().addPoint();
                                            for (Player ps : Bukkit.getOnlinePlayers()) {
                                                Scoreboard.game(ps);
                                            }

                                        }
                                        break;
                                    }
                                }
                            }
                        }
                        LaserPlayer.getPlayer(player.getName()).setShots(LaserPlayer.getPlayer(player.getName()).getShots() + 1);
                        LaserPlayer.getPlayer(player.getName()).setCoolDown(5);
                    }

                }
            }
        }
    }
}
