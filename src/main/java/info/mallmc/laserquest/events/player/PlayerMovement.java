package info.mallmc.laserquest.events.player;


import info.mallmc.framework.api.GameState;
import info.mallmc.laserquest.LaserQuest;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerMovement implements Listener {

    @EventHandler
    public void onEventMove(PlayerMoveEvent e) {
        if (GameState.getGameState() == GameState.IN_GAME) {

            if (e.getPlayer().getLocation().getY() <= 82) {
                if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(e.getPlayer()).getName() == "Red") {
                    LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation1(e.getPlayer());
                } else {
                    LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation2(e.getPlayer());
                }
            }

        }
    }
}
