

package info.mallmc.laserquest.events.player;


import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
/**
 *
 * @author Rushmead
 */
public class PickUpItem implements Listener{

        @EventHandler
        public void onEvent(PlayerPickupItemEvent e){
            e.setCancelled(true);
        }
        @EventHandler
        public void onEvent(PlayerDropItemEvent e){
            e.setCancelled(true);
        }
}
