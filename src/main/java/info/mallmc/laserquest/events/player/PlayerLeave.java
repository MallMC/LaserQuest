package info.mallmc.laserquest.events.player;


import info.mallmc.framework.api.GameState;
import info.mallmc.laserquest.player.LaserPlayer;
import info.mallmc.laserquest.util.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerLeave implements Listener {
    
    @EventHandler
    public void onEventLeave(PlayerQuitEvent e) {
        
      
        LaserPlayer.getLaserDB().savePlayerToDB(LaserPlayer.getPlayer(e.getPlayer().getDisplayName()));
        LaserPlayer.removePlayer(e.getPlayer().getName());
        if(GameState.getGameState() == GameState.LOBBY) {
            for (Player ps : Bukkit.getOnlinePlayers()) {
                Scoreboard.lobby(ps);
            }
        }
    }
}
