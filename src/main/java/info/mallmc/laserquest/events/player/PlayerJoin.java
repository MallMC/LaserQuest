package info.mallmc.laserquest.events.player;

import info.mallmc.framework.api.MallPlayer;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.laserquest.LaserQuest;
import info.mallmc.laserquest.lobby.LobbyItems;
import info.mallmc.laserquest.player.LaserPlayer;
import info.mallmc.laserquest.util.Scoreboard;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

/**
 *
 * @author Rushmead
 */
public class PlayerJoin implements Listener {
    
    @EventHandler
    public void onJoin(PlayerJoinEvent e) {
        LaserPlayer lp = LaserPlayer.createPlayer(e.getPlayer());
        Messaging.broadcastMessage("laserquest.join.broadcast", e.getPlayer().getName(), Bukkit.getOnlinePlayers().size(), 12);
        if (Bukkit.getOnlinePlayers().size() >= 8) {
            Messaging.broadcastMessage("laserquest.join.minium");
            LaserQuest.getInstance().getLobbyManager().startCountdown();
        }
        
        e.getPlayer().teleport(new Location(Bukkit.getWorlds().get(0), -299, 80, -298));
        e.getPlayer().getInventory().clear();
        e.getPlayer().setGameMode(GameMode.SURVIVAL);
        e.getPlayer().getActivePotionEffects().clear();
        for (int i = 0; i < LobbyItems.getInventory().length; i++) {
            if (LobbyItems.getInventory()[i] == null) {
                continue;
            }
            e.getPlayer().getInventory().setItem(i, LobbyItems.getInventory()[i]);
        }
        Scoreboard.lobby(e.getPlayer());
        e.getPlayer().setResourcePack("http://cdn.redstone.tech/Rushmeadfiles/mallmc/LaserQuest-V-1.1.2.zip");
        for (Player ps : Bukkit.getOnlinePlayers()) {
            Scoreboard.lobby(ps);
        }
        MallPlayer ep = MallPlayer.getPlayer(e.getPlayer().getName());
        
        ep.setChatPrefix(ChatColor.GRAY + lp.getGameRank().getDisplayName());
    }
}
