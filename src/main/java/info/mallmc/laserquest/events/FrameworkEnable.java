package info.mallmc.laserquest.events;


import info.mallmc.framework.api.GameState;
import info.mallmc.framework.api.MallCommand;
import info.mallmc.framework.customevents.FrameworkEnableEvent;
import info.mallmc.laserquest.commands.GameCommand;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;

/**
 *
 * @author Rushmead
 */
public class FrameworkEnable implements Listener {

    @EventHandler
    public void onFrameworkEnable(FrameworkEnableEvent e) {
        MallCommand.registerCommand("laserquest", "game", new GameCommand());
        GameState.setGameState(GameState.LOBBY);
    }
}
