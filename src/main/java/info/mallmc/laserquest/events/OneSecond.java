package info.mallmc.laserquest.events;

import info.mallmc.framework.api.GameState;
import info.mallmc.framework.customevents.OneSecondEvent;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.laserquest.LaserQuest;
import info.mallmc.laserquest.player.LaserPlayer;
import info.mallmc.laserquest.util.PlayerUtil;
import net.minecraft.server.v1_10_R1.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

/**
 *
 * @author Rushmead
 */
public class OneSecond implements Listener {

    public static int time = 0;

    @EventHandler
    public void onOneSecond(OneSecondEvent e) {

        time++;
        if (Bukkit.getOnlinePlayers().size() > 0) {

            if (GameState.getGameState() == GameState.SETUP) {

                if (LaserQuest.getInstance().getGameManager().countdown) {
                    if (LaserQuest.getInstance().getGameManager().countDownTime == 0) {
                        LaserQuest.getInstance().getGameManager().start();
                        return;
                    }
                    if (LaserQuest.getInstance().getGameManager().countDownTime <= 10) {
                        for (Player p : Bukkit.getOnlinePlayers()) {
                            p.playSound(p.getLocation(), Sound.UI_BUTTON_CLICK, 1, 1);
                            PlayerUtil.sendTitleOrSubtitle(p, PacketPlayOutTitle.EnumTitleAction.TITLE, "&l" + LaserQuest.getInstance().getGameManager().countDownTime + "", 0, 20, 0, ChatColor.GOLD);
                        }
                    }
                    LaserQuest.getInstance().getGameManager().countDownTime -= 1;
                }

            } else if (GameState.getGameState() == GameState.IN_GAME) {
                if (LaserQuest.getInstance().getGameManager().gameTime == 0) {
                    if (LaserQuest.getInstance().getGameManager().countdown) {
                        if (LaserQuest.getInstance().getGameManager().countDownTime == 30) {
                            Messaging.broadcastMessage("laserquest.game.timer.secondsLeft", LaserQuest.getInstance().getGameManager().countDownTime);
                        }
                        if (LaserQuest.getInstance().getGameManager().countDownTime <= 10) {
                            Messaging.broadcastMessage("laserquest.game.timer.secondsLeft", LaserQuest.getInstance().getGameManager().countDownTime);
                        }
                        if (LaserQuest.getInstance().getGameManager().countDownTime == 0) {
                            LaserQuest.getInstance().getGameManager().stop();
                            return;
                        }

                        LaserQuest.getInstance().getGameManager().countDownTime -= 1;
                    }
                }
                for (LaserPlayer lp : LaserPlayer.getPlayers().values()) {
                    if (lp.isDeactivated()) {

                        lp.setDeactivateTime(lp.getDeactivateTime() - 1);

                        lp.getPlayer().setLevel(lp.getDeactivateTime());

                        if (lp.getDeactivateTime() == 0) {
                            lp.setDeactivated(false);
                            lp.getPlayer().getActivePotionEffects().clear();
                            Messaging.sendMessage(lp.getPlayer(), "laserquest.game.general.reactivate");
                            lp.getPlayer().playSound(lp.getPlayer().getLocation(), Sound.ENTITY_ENDERMEN_SCREAM, 5, 5);
                        }
                    }
                    if (lp.getCoolDown() > 0) {
                        lp.setCoolDown(lp.getCoolDown() - 1);
                        ItemStack im = new ItemStack(Material.WATCH, lp.getCoolDown());
                        ItemMeta imM = im.getItemMeta();
                        imM.setDisplayName(Messaging.colorizeMessage("&cCooldown"));
                        im.setItemMeta(imM);
                        lp.getPlayer().getInventory().setItem(1, im);
                    }
                }
            } else if (GameState.getGameState() == GameState.LOBBY) {
                if (time % 60 == 0) {
                    Messaging.broadcastMessage("laserquest.game.map.playingMap", LaserQuest.getInstance().getMapManager().getCurrentMap().getMapName(), LaserQuest.getInstance().getMapManager().getCurrentMap().getAuthors());
                }
                if (LaserQuest.getInstance().getLobbyManager().countdown) {
                    if (LaserQuest.getInstance().getLobbyManager().countDownTime == 0) {
                        LaserQuest.getInstance().getGameManager().prep();
                        return;
                    }
                    if (LaserQuest.getInstance().getLobbyManager().countDownTime == 60) {
                        Messaging.broadcastMessage("laserquest.game.timer.secondsStart", LaserQuest.getInstance().getLobbyManager().countDownTime);
                    }
                    if (LaserQuest.getInstance().getLobbyManager().countDownTime == 30) {
                        Messaging.broadcastMessage("laserquest.game.timer.secondsStart", LaserQuest.getInstance().getLobbyManager().countDownTime);
                    }
                    if (LaserQuest.getInstance().getLobbyManager().countDownTime <= 10) {
                        Messaging.broadcastMessage("laserquest.game.timer.secondsStart", LaserQuest.getInstance().getLobbyManager().countDownTime);
                    }

                    LaserQuest.getInstance().getLobbyManager().countDownTime -= 1;

                }
            }

        }
    }
}
