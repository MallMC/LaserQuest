package info.mallmc.laserquest.game;


import info.mallmc.framework.messaging.Messaging;
import info.mallmc.laserquest.player.LaserPlayer;
import info.mallmc.laserquest.util.PlayerUtil;
import net.minecraft.server.v1_10_R1.PacketPlayOutTitle;
import org.bukkit.ChatColor;

/**
 * Created by Rushmead for ElasticMC
 */
public enum GameRank {

    PRIVATE(0, 0, "Private"),
    LANCE_CORPORAL(1, 25, "Lance Corporal"),
    SERGEANT(2, 75, "Sergeant"),
    STAFF_SERGEANT(3, 170, "Staff Sergeant"),
    SECOND_LIEUTENANT(4, 260, "Second Lieutenant"),
    LIEUTENANT(5, 360, "Lieutenant"),
    CAPTAIN(6, 500, "Captain"),
    MAJOR(7, 660, "Major"),
    LIEUTENANT_COLONEL(8, 800, "Lieutenant Colonel"),
    COLONEL(9, 1000, "Colonel");
    private int id;
    private int kills;
    private String displayName;

    GameRank(int id, int kills, String displayName) {
        this.id = id;
        this.kills = kills;
        this.displayName = displayName;
    }

    public int getId() {
        return id;
    }

    public int getKills() {
        return kills;
    }

    public String getDisplayName() {
        return displayName;
    }

    public static GameRank getRankFromID(int id) {
        for (GameRank gr : GameRank.values()) {
            if (gr.getId() == id) {
                return gr;
            }
        }
        return GameRank.PRIVATE;
    }

    public static void tryLevelUp(LaserPlayer lp) {
        boolean levelUp = false;
        for (GameRank gr : GameRank.values()) {
            if (lp.getTotalPoints() >= gr.getKills() && gr.getId() > lp.getGameRank().getId() && !levelUp) {
                lp.setGameRank(gr);
                levelUp = true;
            }
        }
        if (levelUp) {
            PlayerUtil.sendTitleOrSubtitle(lp.getPlayer(), PacketPlayOutTitle.EnumTitleAction.TITLE, "Congrats, You Leveled up!", 0, 60, 20, ChatColor.AQUA);
            Messaging.sendMessage(lp.getPlayer(), "laserquest.game.general.levelUp", lp.getGameRank().getDisplayName());
            Messaging.broadcastMessage("laserquest.game.general.leveledUp", lp.getRealName(), lp.getGameRank().getDisplayName());
        }

    }
}
