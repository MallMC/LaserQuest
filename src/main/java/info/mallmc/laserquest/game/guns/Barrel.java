/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.mallmc.laserquest.game.guns;

import info.mallmc.laserquest.game.PartType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Stuart
 */
public enum Barrel {

    NONE(0, "", new ItemStack(Material.AIR), PartType.BARREL, 0, 0),
    DEFAULT(1, "Basic Barrel", new ItemStack(Material.WOOD_SWORD), PartType.BARREL, 1, 0),
    FASTER(2, "Faster Barrel", new ItemStack(Material.STONE_SWORD), PartType.BARREL, 2, 10),
    FAST(3, "Fast Barrel", new ItemStack(Material.IRON_SWORD), PartType.BARREL, 3, 50),
    SLOW(4, "Slow Barrel", new ItemStack(Material.GOLD_SWORD), PartType.BARREL, 4, 100),
    SUPER_SLOW(5, "Super Slow Barrel", new ItemStack(Material.DIAMOND_SWORD), PartType.BARREL, 5, 1000);
    private int id;
    private String name;
    private ItemStack item;
    private PartType type;
    private int damage;
    private int cost;

    Barrel(int id, String name, ItemStack item, PartType type, int damage, int cost) {
        this.id = id;
        this.name = name;
        this.item = item;
        this.type = type;
        this.damage = damage;
        this.cost = cost;

    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the item
     */
    public ItemStack getItem() {
        return item;
    }

    /**
     * @return the type
     */
    public PartType getType() {
        return type;
    }

    /**
     * @return the damage
     */
    public int getDamage() {
        return damage;
    }

    public static Barrel getBarrelFromID(int id) {
        for (Barrel b : Barrel.values()) {
            if (b.getId() == id) {
                return b;
            }
        }
        return Barrel.DEFAULT;
    }

    /**
     * @return the cost
     */
    public int getCost() {
        return cost;
    }
}
