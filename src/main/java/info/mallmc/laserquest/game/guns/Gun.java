/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.mallmc.laserquest.game.guns;

/**
 *
 * @author Stuart
 */
public class Gun {

    private String name;
    private Barrel barrel;
    private Case gunCase;
    private Cooler cooler;
    private Laser laser;

    public Gun(String name, Barrel barrel, Case gunCase, Cooler cooler, Laser laser) {

        this.name = name;
        this.barrel = barrel;
        this.gunCase = gunCase;
        this.cooler = cooler;
        this.laser = laser;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the barrel
     */
    public Barrel getBarrel() {
        return barrel;
    }

    /**
     * @return the gunCase
     */
    public Case getGunCase() {
        return gunCase;
    }

    /**
     * @return the cooler
     */
    public Cooler getCooler() {
        return cooler;
    }

    /**
     * @return the laser
     */
    public Laser getLaser() {
        return laser;
    }

    public String asString() {
        return barrel.getId() + ";" + gunCase.getId() + ";" + cooler.getId() + ";" + laser.getId() + ";" + name;
    }

    public static Gun getGunByString(String s) {
        String[] ids = s.split(";");
        Barrel b = Barrel.getBarrelFromID(Integer.parseInt(ids[0]));
        Case c = Case.getCaseFromID(Integer.parseInt(ids[1]));
        Cooler cooler = Cooler.getCoolerFromID(Integer.parseInt(ids[2]));
        Laser laser = Laser.getLaserFromID(Integer.parseInt(ids[3]));
        String name = ids[4];

        return new Gun(name, b, c, cooler, laser);
    }

    public static Gun getDefault() {
        return new Gun("Basic Shooter", Barrel.DEFAULT, Case.DEFAULT, Cooler.DEFAULT, Laser.DEFAULT);
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @param barrel the barrel to set
     */
    public void setBarrel(Barrel barrel) {
        this.barrel = barrel;
    }

    /**
     * @param gunCase the gunCase to set
     */
    public void setGunCase(Case gunCase) {
        this.gunCase = gunCase;
    }

    /**
     * @param cooler the cooler to set
     */
    public void setCooler(Cooler cooler) {
        this.cooler = cooler;
    }

    /**
     * @param laser the laser to set
     */
    public void setLaser(Laser laser) {
        this.laser = laser;
    }

}
