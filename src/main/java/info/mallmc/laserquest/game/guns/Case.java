/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.mallmc.laserquest.game.guns;

import info.mallmc.laserquest.game.PartType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Stuart
 */
public enum Case {

    NONE(0, null, null, null, Material.AIR, 0),
    DEFAULT(1, "Basic Case", new ItemStack(Material.WOOL, (byte) 15), PartType.CASE, Material.IRON_HOE, 0);
    private int id;
    private String name;
    private ItemStack item;
    private PartType type;
    private Material material;
    private int cost;

    Case(int id, String name, ItemStack item, PartType type, Material material, int cost) {
        this.id = id;
        this.name = name;
        this.item = item;
        this.type = type;
        this.material = material;
        this.cost = cost;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the item
     */
    public ItemStack getItem() {
        return item;
    }

    /**
     * @return the type
     */
    public PartType getType() {
        return type;
    }

    /**
     * @return the material
     */
    public Material getMaterial() {
        return material;
    }

    public static Case getCaseFromID(int id) {
        for (Case b : Case.values()) {
            if (b.getId() == id) {
                return b;
            }
        }
        return Case.NONE;
    }

    /**
     * @return the cost
     */
    public int getCost() {
        return cost;
    }
}
