/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.mallmc.laserquest.game.guns;

import info.mallmc.laserquest.game.PartType;
import org.bukkit.Color;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Stuart
 */
public enum Laser {

    NONE(0, null, null, null, Color.WHITE, 0, null),
    DEFAULT(1, "Basic Laser", new ItemStack(Material.STAINED_GLASS), PartType.LASER, Color.WHITE, 0, DyeColor.WHITE),
    ORANGE(2, "Orange Laser", new ItemStack(Material.STAINED_GLASS), PartType.LASER, Color.ORANGE, 10, DyeColor.ORANGE),
    BLUE(3, "Blue Laser", new ItemStack(Material.STAINED_GLASS), PartType.LASER, Color.BLUE, 10, DyeColor.BLUE),
    YELLOW(4, "Yellow Laser", new ItemStack(Material.STAINED_GLASS), PartType.LASER, Color.YELLOW, 10, DyeColor.YELLOW),
    GREEN(5, "Green Laser", new ItemStack(Material.STAINED_GLASS), PartType.LASER, Color.GREEN, 10, DyeColor.GREEN),
    PURPLE(6, "Purple Laser", new ItemStack(Material.STAINED_GLASS), PartType.LASER, Color.PURPLE, 10, DyeColor.PURPLE),
    PINK(7, "Pink Laser", new ItemStack(Material.STAINED_GLASS), PartType.LASER, Color.FUCHSIA, 10, DyeColor.PINK);
    private int id;
    private String name;
    private ItemStack item;
    private PartType type;
    private Color color;
    private int cost;
    private DyeColor itemColour;

    Laser(int id, String name, ItemStack item, PartType type, Color color, int cost, DyeColor itemColour) {
        this.id = id;
        this.name = name;
        this.item = item;
        this.type = type;
        this.color = color;
        this.cost = cost;
        this.itemColour = itemColour;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the item
     */
    public ItemStack getItem() {
        return item;
    }

    /**
     * @return the type
     */
    public PartType getType() {
        return type;
    }

    /**
     * @return the color
     */
    public Color getColor() {
        return color;
    }

    public static Laser getLaserFromID(int id) {
        for (Laser b : Laser.values()) {
            if (b.getId() == id) {
                return b;
            }
        }
        return Laser.NONE;
    }

    /**
     * @return the cost
     */
    public int getCost() {
        return cost;
    }

    /**
     * @return the itemColour
     */
    public DyeColor getItemColour() {
        return itemColour;
    }
}
