/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.mallmc.laserquest.game.guns;

import info.mallmc.laserquest.game.PartType;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

/**
 *
 * @author Stuart
 */
public enum Cooler {

    NONE(0, null, null, null, 0, 0),
    DEFAULT(1, "Basic Cooler", new ItemStack(Material.WATER_BUCKET), PartType.COOLER, 4, 0);
    private int id;
    private String name;
    private ItemStack item;
    private PartType type;
    private int cooldown;
    private int cost;

    Cooler(int id, String name, ItemStack item, PartType type, int cooldown, int cost) {
        this.id = id;
        this.name = name;
        this.item = item;
        this.type = type;
        this.cooldown = cooldown;
        this.cost = cost;
    }

    /**
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @return the item
     */
    public ItemStack getItem() {
        return item;
    }

    /**
     * @return the type
     */
    public PartType getType() {
        return type;
    }

    /**
     * @return the cooldown
     */
    public int getCooldown() {
        return cooldown;
    }

    public static Cooler getCoolerFromID(int id) {
        for (Cooler b : Cooler.values()) {
            if (b.getId() == id) {
                return b;
            }
        }
        return Cooler.NONE;
    }

    /**
     * @return the cost
     */
    public int getCost() {
        return cost;
    }
}
