package info.mallmc.laserquest.game;

import info.mallmc.framework.api.GameState;
import info.mallmc.framework.api.MallPlayer;
import info.mallmc.framework.database.Players;
import info.mallmc.framework.messaging.Messaging;
import info.mallmc.laserquest.LaserQuest;
import info.mallmc.laserquest.game.guns.Gun;
import info.mallmc.laserquest.player.LaserPlayer;
import info.mallmc.laserquest.teams.Team;
import info.mallmc.laserquest.util.PlayerUtil;
import info.mallmc.laserquest.util.Scoreboard;
import net.minecraft.server.v1_10_R1.PacketPlayOutTitle;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.util.ChatPaginator;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Rushmead
 */
public class GameManager {
    
    public boolean countdown = false;
    public int countDownTime = 10;
    // In Minutes
    public int gameTime = 5;
    
    public void start() {
        for (MallPlayer ep : MallPlayer.getPlayers().values()) {
            ep.setFrozen(false);
            
            PlayerUtil.sendTitleOrSubtitle(Bukkit.getPlayer(ep.getUserName()), PacketPlayOutTitle.EnumTitleAction.TITLE, "GAME STARTED", 0, 20, 0, ChatColor.DARK_PURPLE);
        }
        Messaging.broadcastMessage("laserquest.game.general.started");
        
        GameState.setGameState(GameState.IN_GAME);
        countdown = false;
    }
    
    public void stop() {
        GameState.setGameState(GameState.ENDING);
        Team winner = null;
        String bullet = "\u2022";
        String bullets = ChatColor.DARK_GRAY + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet + bullet;
        if (LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getPoints() > LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getPoints()) {
            winner = LaserQuest.getInstance().getTeamManager().getTeamByName("Red");
        } else if (LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getPoints() == LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getPoints()) {
            
        } else {
            winner = LaserQuest.getInstance().getTeamManager().getTeamByName("Green");
        }
        Messaging.broadcastMessage("laserquest.game.general.ended");
        for (MallPlayer ep : MallPlayer.getPlayers().values()) {
            Player p = Bukkit.getPlayer(ep.getUserName());
            p.getInventory().clear();
            
            PlayerUtil.sendTitleOrSubtitle(p, PacketPlayOutTitle.EnumTitleAction.TITLE, "GAME ENDED!", 0, 20, 0, ChatColor.DARK_PURPLE);
        }
        Messaging.broadcastCustomRawMessage(title(Messaging.getMessage("laserquest.game.end.bullets")));
        if (winner == null) {
            Messaging.broadcastCustomRawMessage(title(Messaging.getMessage("laserquest.game.end.tie")));
        } else {
            Messaging.broadcastCustomRawMessage(title(Messaging.getMessage("laserquest.game.end.winner")), winner.getColor(), winner.getName());
        }
        Messaging.broadcastCustomRawMessage(title(Messaging.getMessage("laserquest.game.end.teamPoints")), LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getColor(), LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getName(), LaserQuest.getInstance().getTeamManager().getTeamByName("Red").getPoints());
        Messaging.broadcastCustomRawMessage(title(Messaging.getMessage("laserquest.game.end.teamPoints")), LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getColor(), LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getName(), LaserQuest.getInstance().getTeamManager().getTeamByName("Green").getPoints());
        for (Player p : Bukkit.getOnlinePlayers()) {
            Messaging.sendCustomRawMessage(p, title(Messaging.getMessage("laserquest.game.end.playersCaught")), ChatColor.GOLD, "Your", LaserPlayer.getPlayer(p.getName()).getPoints());
            Messaging.sendCustomRawMessage(p, title(Messaging.getMessage("laserquest.game.end.currencyEarnt")), ChatColor.GOLD, "Your", LaserPlayer.getPlayer(p.getName()).getPoints() / 5);
            int current = Players.getPlayerCurrency().getCurrency(p.getName());
            Players.getPlayerCurrency().setCurrency(p.getName(), current + (LaserPlayer.getPlayer(p.getName()).getPoints() / 5));
            LaserPlayer lp = LaserPlayer.getPlayer(p.getName());
            lp.setGames(lp.getGames() + 1);
            lp.setTotalPoints(lp.getTotalPoints() + lp.getPoints());
            GameRank.tryLevelUp(LaserPlayer.getPlayer(p.getName()));
        }
        Messaging.broadcastCustomRawMessage(title(Messaging.getMessage("laserquest.game.end.restarting")));
        Messaging.broadcastCustomRawMessage(title(Messaging.getMessage("laserquest.game.end.bullets")));
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "restartserver 10");
    }
    
    public String title(String text) {
        
        String title = "";
        
        for (int x = 0; x <= ((ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH / 2) - text.length()); x++) {
            title += " ";
        }
        
        title += text;
        
        for (int x = 0; x <= ((ChatPaginator.AVERAGE_CHAT_PAGE_WIDTH / 2) - text.length()); x++) {
            title += " ";
        }
        
        return title;
        
    }
    
    public void startCountdown() {
        GameState.setGameState(GameState.SETUP);
        countdown = true;
    }
    
    public org.bukkit.scoreboard.Team getSmallestTeam() {
        org.bukkit.scoreboard.Team smallest = (org.bukkit.scoreboard.Team) Scoreboard.getScoreboard().getTeams().toArray()[0];
        for (org.bukkit.scoreboard.Team t : Scoreboard.getScoreboard().getTeams()) {
            if (Scoreboard.getScoreboard().getTeam(t.getName()).getEntries().size() < Scoreboard.getScoreboard().getTeam(smallest.getName()).getEntries().size()) {
                smallest = t;
            }
        }
        return smallest;
    }
    
    public void prep() {
        
        for (Player p : Bukkit.getOnlinePlayers()) {
            LaserPlayer lp = LaserPlayer.getPlayer(p.getDisplayName());
            p.getInventory().clear();
            if (LaserQuest.getInstance().getTeamManager().hasTeam(p)) {
                lp.setTeam(LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p));
            } else {
                
                lp.setTeam(LaserQuest.getInstance().getTeamManager().getTeamByName(getSmallestTeam().getName()));
                LaserQuest.getInstance().getTeamManager().getTeamByName(getSmallestTeam().getName()).addPlayer(p);
            }
            
            LaserQuest.getInstance().getTeamManager().getScoreboardTeams().get(LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p)).addPlayer(p);
            if (LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getName() == "Red") {
                LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation1(p);
            } else {
                LaserQuest.getInstance().getMapManager().getCurrentMap().teleportPlayerToLocation2(p);
            }
            p.setScoreboard(Scoreboard.getScoreboard());
            Gun gt = lp.getGun();
            if (gt == null) {
                
            } else {
                
                ItemStack gun = new ItemStack(gt.getGunCase().getMaterial());
                ItemMeta gunMeta = gun.getItemMeta();
                gunMeta.setDisplayName(ChatColor.GOLD + gt.getName());
                List<String> lore = new ArrayList<>();
                lore.add(LaserQuest.getInstance().getTeamManager().getTeamFromPlayer(p).getColor() + gt.getName());
                lore.add(Messaging.colorizeMessage("&4Barrel: &6&l" + gt.getBarrel().getName()));
                lore.add(Messaging.colorizeMessage("&4Cooler: &6&l" + gt.getCooler().getName()));
                lore.add(Messaging.colorizeMessage("&4Case: &6&l" + gt.getGunCase().getName()));
                lore.add(Messaging.colorizeMessage("&4Laser: &6&l" + gt.getLaser().getName()));
                gunMeta.setLore(lore);
                gun.setItemMeta(gunMeta);
                p.getInventory().addItem(gun);
                Scoreboard.game(p);
            }
        }
        for (MallPlayer ep : MallPlayer.getPlayers().values()) {
            ep.setFrozen(true);
        }
        
        startCountdown();
    }
}
